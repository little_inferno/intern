package intern

import cats.effect.IO

/** Нужно values преобразовать в Result, сгруппировав успешные и не успешные вычисления Сохранить порядок следования
  * значений
  */
object Task extends App {
  val values: List[IO[Int]] =
    List(IO.pure(1), IO.pure(2), IO.raiseError(new Throwable("one")), IO.pure(3), IO.raiseError(new Throwable("two")))

  case class Result(success: List[Int], errors: List[Throwable])
}
