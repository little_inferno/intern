ThisBuild / version := "0.1.0"

ThisBuild / scalaVersion := "2.13.14"

lazy val root = (project in file("."))
  .settings(name := "intern")

libraryDependencies ++= Seq("org.typelevel" %% "cats-effect" % "3.5.4")

ThisBuild / scalacOptions ++= Seq(
  "-feature",
  "-language:implicitConversions",
  "-Xfatal-warnings",
  "-Ymacro-annotations"
)
